# gitlab runner

## デプロイ手順

### namespaceの作成

gitlab runnerで用いるnamespaceは`gitlab-runner`です。

```shell
kubectl apply -f namespace.yaml
```

### gitlab runnerのデプロイ

[helm chartによるデプロイ](https://docs.gitlab.com/runner/install/kubernetes.html#installing-gitlab-runner-using-the-helm-chart)

```shell
helm install --namespace gitlab-runner gitlab-runner -f ./helm-configure.yaml gitlab/gitlab-runner
helm upgrade --namespace gitlab-runner gitlab-runner -f ./helm-configure.yaml gitlab/gitlab-runner
```

