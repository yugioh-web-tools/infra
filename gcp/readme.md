# Yugioh Web Toolsのインフラ

GitLab CI, デプロイなど利用します。

[Console is Here](https://console.cloud.google.com/home/dashboard?hl=ja&organizationId=0&project=yugioh-web-tools)

# デプロイ方法

```shell
gcloud deployment-manager deployments create infra --config infra.yaml
gcloud deployment-manager deployments delete infra
gcloud deployment-manager deployments update infra --config infra.yaml
```

# デプロイしたクラスタの利用

## コンソール

[Kubernetes Cluster](https://console.cloud.google.com/kubernetes/clusters/details/us-east1-b/gke/details?hl=ja&organizationId=0&project=yugioh-web-tools)

## kubectlコマンドラインアクセス

```shell
gcloud container clusters get-credentials gke --zone us-east1-b --project yugioh-web-tools
```
